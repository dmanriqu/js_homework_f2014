# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Category.delete_all
SalesItem.delete_all

cats = %w{Clothing Furniture Housewares}
cats.each do |c|
  Category.create(name:c,notes: "#{c} located on aisle #{(rand * 10).to_i}")
end

clothing = Category.find_by_name("Clothing")
%w{Tshirt Pants Gloves}.each do |item|
  SalesItem.create(name:item,category:clothing,price: rand * 100)
end