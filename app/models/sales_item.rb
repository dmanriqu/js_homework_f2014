class SalesItem < ActiveRecord::Base

  belongs_to :category
  def self.average_price
     SalesItem.average('price') || 0.00
  end
end
